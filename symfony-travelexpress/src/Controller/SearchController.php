<?php

namespace App\Controller;

use App\Entity\Profile;
use App\Entity\Travel;

use App\Form\SearchType;
use App\Form\NewTravelType;
use App\Form\CheckoutType;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    private $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @Route("/", name="search")
     */
    public function index(Request $request): Response
    {
        // Getting session
        $session = $this->requestStack->getSession();
        $connected_session = $session->get('connected');

        // Making front-end form
        $form = $this->createForm(SearchType::class, array('method' => 'GET'));
        $form->handleRequest($request);
        // Getting available travel
        $travels = $this->getDoctrine()
            ->getRepository(Travel::class)
            ->findAvailable()
        ;

        if ($connected_session == "true") {
            $user_id = $session->get('user_id');
        } else $user_id = null;

        // Form sent statement
        if ($form->isSubmitted() && $form->isValid()) {
            $travels = $this->getDoctrine()
                ->getRepository(Travel::class)
                ->findByDepartureAndArrival($form->get('departure')->getData(), $form->get('arrival')->getData())
            ;
        }
        // Rendering
        return $this->render('search/index.html.twig', [
            'travels' => $travels,
            'search_form' => $form->createView(),
            'user_id' => $user_id,
        ]);
    }

    /**
     * @Route("/travel/new", name="newTravel")
     */
    public function newTravel(Request $request): Response
    {
        // Getting session
        $session = $this->requestStack->getSession();
        $connected_session = $session->get('connected');
        // User logged statement
        if ($connected_session == "true")
        {
            // Making front-end form
            $travel = new Travel();
            $form = $this->createForm(NewTravelType::class, $travel);
            $form->handleRequest($request);
            $user_id = $session->get('user_id');

            // Form sent statement
            if ($form->isSubmitted() && $form->isValid()) {
                // Adding new travel entry to database
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($travel);
                $entityManager->flush();
                // Setting user id for new travel entry
                $user_id = $session->get('user_id');
                $travel_id = $form->getData()->getId();
                $initUserQuery = "UPDATE travel SET user_driver_id = :user_id, profile_id = :user_id WHERE id = :travel_id";
                $statement = $entityManager->getConnection()->prepare($initUserQuery);
                $statement->bindValue('user_id', $user_id);
                $statement->bindValue('travel_id', $travel_id);
                $statement->execute();

                // Redirecting
                return $this->redirectToRoute('search');
            }
            // Form isn't sent statement
            else {
                return $this->render('search/new.html.twig', [
                    'travel' => $travel,
                    'new_travel_form' => $form->createView(),
                    'user_id' => $user_id,
                ]);
            }
        }
        // User doesn't logged statement
        else
        {
            // Redirecting to log in page
            return $this->redirectToRoute('login');
        }
    }

    /**
     * @Route("/travel", name="travel")
     */
    public function travel(): Response
    {
        // Getting session
        $session = $this->requestStack->getSession();
        $connected_session = $session->get('connected');

        if ($connected_session == "true") {
            $user_id = $session->get('user_id');
        } else $user_id = null;

        $travels = $this->getDoctrine()
            ->getRepository(Travel::class)
            ->findAvailable()
        ;

        return $this->render('search/travel.html.twig', [
            'listtravel' => $travels,
            'user_id' => $user_id,
        ]);
    }

    /**
     * @Route("/book/{id}", name="book")
     */
    public function book($id, Request $request): Response
    {
        // Getting session
        $session = $this->requestStack->getSession();
        $connected_session = $session->get('connected');
        // User logged statement
        if ($connected_session == "true")
        {
            // Making front-end form
            $checkout_form = $this->createForm(CheckoutType::class);
            $checkout_form->handleRequest($request);
            $user_id = $session->get('user_id');
            // Getting travel information in database
            $travel = $this->getDoctrine()
                ->getRepository(Travel::class)
                ->find($id);
            if($checkout_form->isSubmitted() && $checkout_form->isValid())
            {
                $seat_available = $travel->getSeatAvailable();
                $seat_wanted = $checkout_form->get('seat_wanted')->getData();
                // Enough seat available statement
                if ($seat_wanted <= $seat_available)
                {
                    return $this->redirectToRoute('checkout',[
                        'id' => $id,
                        'seat_wanted' => $seat_wanted,
                    ]);
                }
                else
                {
                    return $this->render('search/book.html.twig', [
                        'travel' => $travel,
                        'checkout_form' => $checkout_form->createView(),
                        'error' => 1,
                        'user_id' => $user_id,
                    ]);
                }
            }
            // Rendering
            return $this->render('search/book.html.twig', [
                'travel' => $travel,
                'checkout_form' => $checkout_form->createView(),
                'error' => 0,
                'user_id' => $user_id,
            ]);
        }
        // User doesn't logged statement
        else
        {
            // Redirecting to log in page
            return $this->redirectToRoute('login');
        }
    }

    /**
     * @Route("/checkout/{id}/{seat_wanted}", name="checkout")
     */
    public function checkout($id, $seat_wanted)
    {
        // Getting session
        $session = $this->requestStack->getSession();
        $connected_session = $session->get('connected');
        // User logged statement
        if ($connected_session == "true")
        {
            // Getting travel information in database
            $travel = $this->getDoctrine()
                ->getRepository(Travel::class)
                ->find($id);
            // Getting seat available value of travel
            $seat_available = $travel->getSeatAvailable();
            $user_id = $session->get('user_id');
            // Available seat statement
            if ($seat_wanted <= $seat_available)
            {
                // Decrementing travel available place in database
                $new_seat_available = $seat_available - $seat_wanted;
                $decrementSeatAvailableQuery = "UPDATE travel SET seat_available = :new_seat_available WHERE id = :travel_id";
                $entityManager = $this->getDoctrine()->getManager();
                $statement = $entityManager->getConnection()->prepare($decrementSeatAvailableQuery);
                $statement->bindValue('new_seat_available', $new_seat_available);
                $statement->bindValue('travel_id', $id);
                $statement->execute();
                //Rendering
                return $this->render('search/checkout.html.twig', [
                    'travel' => $travel,
                    'seat_wanted' => $seat_wanted,
                    'error' => 0,
                    'user_id' => $user_id,
                ]);
            }
            // Seat no longer available statement
            else
            {
                return $this->render('search/checkout.html.twig', [
                    'travel' => $travel,
                    'seat_wanted' => $seat_wanted,
                    'error' => 1,
                    'user_id' => $user_id,
                ]);
            }
        }
        // User doesn't logged statement
        else
        {
            // Redirecting to log in page
            return $this->redirectToRoute('login');
        }
    }
}