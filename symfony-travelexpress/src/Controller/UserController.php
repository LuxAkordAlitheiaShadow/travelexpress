<?php

namespace App\Controller;

use App\Entity\Profile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

use App\Entity\User;

use App\Form\UserType;
use App\Form\LoginType;

class UserController extends AbstractController
{
    private $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @Route("/user/login", name="login")
     */
    public function login(Request $request): Response
    {
        // Making front-end form
        $user = new User();
        $login_form = $this->createForm(LoginType::class, $user);
        $login_form->handleRequest($request);
        $user_id = null;

        // Form sent statement
        if ($login_form->isSubmitted() && $login_form->isValid())
        {
            // Extracting credentials from form
            $mail = $login_form->get("mail")->getData();
            $hash_pwd = $login_form->get("hash_pwd")->getData();
            // Requesting database with credentials
            $credentials = $this->getDoctrine()
                ->getRepository(User::class)
                ->findValidLogin($mail, $hash_pwd);
            // Credentials match statement
            if ($credentials)
            {
                $user_id = $credentials[0]['id'];
                // Creating session token
                $session = $this->requestStack->getSession();
                $session->set('connected', 'true'); //TODO improve session attribute for security reason
                $session->set('user_id', $user_id);
                return $this->redirectToRoute('search');
            }
            // Credentials doesn't statement
            else
            {
                return $this->render('user/login.html.twig', [
                    'user' => $user,
                    'login_form' => $login_form->createView(),
                    'error' => 1,
                    'user_id' => $user_id,
                ]);
            }
        }
        return $this->render('user/login.html.twig', [
            'user' => $user,
            'login_form' => $login_form->createView(),
            'error' => 0,
            'user_id' => $user_id,
        ]);
    }

    /**
     * @Route("/user/logout", name="logout")
     */
    public function logout(Request $request): Response
    {
        // Log out user and unset session
        $session = $this->requestStack->getSession();
        $session->set('connected', 'false'); //TODO improve session attribute for security reason
        $session->set('user_id', 'null');
        return $this->redirectToRoute('login');
    }

    /**
     * @Route("/user/new", name="new_user")
     */
    public function new_user(Request $request): Response
    {
        // Making front-end form
        $user = new User();
        $user_form = $this->createForm(UserType::class, $user);
        $user_form->handleRequest($request);
        $user_id = null;

        // Form sent statement
        if ($user_form->isSubmitted() && $user_form->isValid())
        {
            // Pushing in database new user and profile entries
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // Setting user point to default value
//            $user_id = $user->getId();
//            $initPointQuery = "UPDATE profile SET point = 0 WHERE id = :user_id";
//            $statement = $entityManager->getConnection()->prepare($initPointQuery);
//            $statement->bindValue('user_id', $user_id);
//            $statement->execute();

            return $this->redirectToRoute('search');
        }

        return $this->render('user/new_user.html.twig', [
            'user' => $user,
            'user_form' => $user_form->createView(),
            'user_id' => $user_id,
        ]);
    }

    /**
     * @Route("/user/", name="user")
     */
    public function user(): Response
    {
        // Getting session
        $session = $this->requestStack->getSession();
        $connected_session = $session->get('connected');

        if ($connected_session == "true")
        {
            $user_id = $session->get('user_id');
            /*$preference = $this->getDoctrine()
                ->getRepository(User::class)
                ->findPreference($user_id);*/
            $user = $this->getDoctrine()
                ->getRepository(User::class)
                ->find($user_id);

            $profile = $this->getDoctrine()
                ->getRepository(Profile::class)
                ->find($user_id);

            $smoke = $profile->getSmoke();
            $animal = $profile->getAnimal();

            return $this->render('user/user.html.twig', [
                'user_id' => $user_id,
                'user' => $user,
                'smoke' => $smoke,
                'animal' => $animal,
            ]);
        }
        // User doesn't logged statement
        else
        {
            // Redirecting to log in page
            return $this->redirectToRoute('login');
        }
    }
}