<?php

namespace App\Form;

use App\Entity\User;

use App\Form\ProfileType;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class,
                [
                    'attr' => [
                        'class' => 'appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-md',
                        'placeholder' => 'Nom',
                    ]
                ])
            ->add('surname', TextType::class,
                [
                    'attr' => [
                        'class' => 'appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-md',
                        'placeholder' => 'Prénom',
                    ]
                ])
            ->add('mail', EmailType::class,
                [
                    'attr' => [
                        'class' => 'appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-md',
                        'placeholder' => 'E-mail',
                    ]
                ])
            ->add('hash_pwd', PasswordType::class,
                [
                    'attr' => [
                        'class' => 'appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-md',
                        'placeholder' => 'Mot de passe',
                    ]
                ])
            ->add('have_preference', ProfileType::class,
                [
                    'attr' => [
                        'class' => 'appearance-none checked:bg-blue-600 checked:border-transparent',
                        'placeholder' => 'Préférences',
                    ]
                ])
        ;


    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
