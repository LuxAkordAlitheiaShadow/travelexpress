<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\Profile;
use App\Entity\Travel;
use App\Entity\Car;
use App\Entity\User;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewTravelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('departure_datetime', DateTimeType::class,
                [
                    'widget' => 'single_text',
                    'attr' => [
                        'class' => 'mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm',
                    ]
                ])
            ->add('seat_available', IntegerType::class,
                [
                    'attr' => [
                        'class' => 'mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm',
                    ]
                ])
            ->add('drive_to', EntityType::class,
                [
                    'class' => City::class,
                    'choice_label' => 'city_name',
                    'placeholder' => 'Ville d\'arrivée',
                    'attr' => [
                        'class' => 'mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm',
                    ]
                ])
            ->add('drive_from', EntityType::class,
                [
                    'class' => City::class,
                    'choice_label' => 'city_name',
                    'placeholder' => 'Ville de départ',
                    'attr' => [
                        'class' => 'mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm',
                    ]
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Travel::class,
        ]);
    }
}
