<?php

namespace App\Form;

use App\Entity\City;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class SearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('departure', EntityType::class,
                [
                    'class' => City::class,
                    'choice_label' => 'city_name',
                    'placeholder' => 'Ville de départ',
                    'attr' => [
                        'class' => 'mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm',
                    ]
                ])
            ->add('arrival', EntityType::class,
                [
                    'class' => City::class,
                    'choice_label' => 'city_name',
                    'placeholder' => 'Ville d\'arrivée',
                    'attr' => [
                        'class' => 'mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm',
                    ]
                ])
        ;
    }
}
