<?php

namespace App\Repository;

use App\Entity\Profile;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findValidLogin($mail, $hash_pwd)
    {
        return $this->createQueryBuilder('u')
            ->where('u.mail = :mail')
            ->setParameter('mail', $mail)
            ->andWhere('u.hash_pwd = :hash_pwd')
            ->setParameter('hash_pwd', $hash_pwd)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
    }

    public function findPreference($id)
    {
        return $this->createQueryBuilder('u')
            ->innerJoin(Profile::class, 'p', 'WITH', 'p.id = u.id')
            ->where('u.id = :id')
            ->setParameter('id', $id)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
    }
}
