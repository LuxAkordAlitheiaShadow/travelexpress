<?php

namespace App\Repository;

use App\Entity\Travel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Travel|null find($id, $lockMode = null, $lockVersion = null)
 * @method Travel|null findOneBy(array $criteria, array $orderBy = null)
 * @method Travel[]    findAll()
 * @method Travel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TravelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Travel::class);
    }

    public function findByDepartureAndArrival($departure, $arrival)
    {
        return $this->createQueryBuilder('t')
            ->where('t.drive_from = :departure')
            ->andWhere('t.drive_to = :arrival')
            ->setParameters(array('departure' => $departure, 'arrival' => $arrival))
            ->orderBy('t.departure_datetime', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
            ;
    }

    public function selectSeatAvailable($id)
    {
        $travel = $this->createQueryBuilder('t')
            ->where('t.id = :travel_id')
            ->setParameters(array('travel_id' => $id))
            ->setMaxResults(1)
            ->getQuery()
            ->getArrayResult()
            ;
        return $travel[0]['seat_available'];
    }

    public function findAvailable()
    {
        return $this->createQueryBuilder('t')
            ->where('t.seat_available > 0')
            ->orderBy('t.departure_datetime', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
            ;
    }
}
