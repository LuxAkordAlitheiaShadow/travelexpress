<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\TravelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TravelRepository::class)
 */
class Travel
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\GreaterThanOrEqual("today")
     */
    private $departure_datetime;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(
     *      min = 1,
     *      max = 60,
     *      notInRangeMessage = "You must have between {{ min }} and {{ max }} seats available",
     * )
     */
    private $seat_available;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="purpose_travel")
     * @ORM\JoinColumn(nullable=true)
     */
    private $user_driver;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="is_passenger")
     */
    private $user_passenger;

    /**
     * @ORM\ManyToOne(targetEntity=Profile::class, inversedBy="set_preference")
     * @ORM\JoinColumn(nullable=true)
     */
    private $profile;

    /**
     * @ORM\ManyToOne(targetEntity=City::class, inversedBy="travel")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank
     */
    private $drive_to;

    /**
     * @ORM\ManyToOne(targetEntity=City::class, inversedBy="travel")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank
     */
    private $drive_from;

    public function __construct()
    {
        $this->user_passenger = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDepartureDatetime(): ?\DateTimeInterface
    {
        return $this->departure_datetime;
    }

    public function setDepartureDatetime(\DateTimeInterface $departure_datetime): self
    {
        $this->departure_datetime = $departure_datetime;

        return $this;
    }

    public function getSeatAvailable(): ?int
    {
        return $this->seat_available;
    }

    public function setSeatAvailable(int $seat_available): self
    {
        $this->seat_available = $seat_available;

        return $this;
    }

    public function getUserDriver(): ?User
    {
        return $this->user_driver;
    }

    public function setUserDriver(?User $user_driver): self
    {
        $this->user_driver = $user_driver;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUserPassenger(): Collection
    {
        return $this->user_passenger;
    }

    public function addUserPassenger(User $userPassenger): self
    {
        if (!$this->user_passenger->contains($userPassenger)) {
            $this->user_passenger[] = $userPassenger;
            $userPassenger->addIsPassenger($this);
        }

        return $this;
    }

    public function removeUserPassenger(User $userPassenger): self
    {
        if ($this->user_passenger->removeElement($userPassenger)) {
            $userPassenger->removeIsPassenger($this);
        }

        return $this;
    }

    public function getProfile(): ?Profile
    {
        return $this->profile;
    }

    public function setProfile(?Profile $profile): self
    {
        $this->profile = $profile;

        return $this;
    }

    public function getCar(): ?Car
    {
        return $this->car;
    }

    public function setCar(?Car $car): self
    {
        $this->car = $car;

        return $this;
    }

    public function getDriveTo(): ?City
    {
        return $this->drive_to;
    }

    public function setDriveTo(?City $drive_to): self
    {
        $this->drive_to = $drive_to;

        return $this;
    }

    public function getDriveFrom(): ?City
    {
        return $this->drive_from;
    }

    public function setDriveFrom(?City $drive_from): self
    {
        $this->drive_from = $drive_from;

        return $this;
    }
}
