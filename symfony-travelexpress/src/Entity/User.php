<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mail;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $hash_pwd;

    /**
     * @ORM\OneToMany(targetEntity=Travel::class, mappedBy="user_driver")
     */
    private $purpose_travel;

    /**
     * @ORM\ManyToMany(targetEntity=Travel::class, inversedBy="user_passenger")
     */
    private $is_passenger;

    /**
     * @ORM\OneToOne(targetEntity=Profile::class, cascade={"persist", "remove"})
     */
    private $have_preference;

    public function __construct()
    {
        $this->purpose_travel = new ArrayCollection();
        $this->is_passenger = new ArrayCollection();
        $this->can_drive_with = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    public function getHashPwd(): ?string
    {
        return $this->hash_pwd;
    }

    public function setHashPwd(string $hash_pwd): self
    {
        $this->hash_pwd = $hash_pwd;

        return $this;
    }

    /**
     * @return Collection|Travel[]
     */
    public function getPurposeTravel(): Collection
    {
        return $this->purpose_travel;
    }

    public function addPurposeTravel(Travel $purposeTravel): self
    {
        if (!$this->purpose_travel->contains($purposeTravel)) {
            $this->purpose_travel[] = $purposeTravel;
            $purposeTravel->setUserDriver($this);
        }

        return $this;
    }

    public function removePurposeTravel(Travel $purposeTravel): self
    {
        if ($this->purpose_travel->removeElement($purposeTravel)) {
            // set the owning side to null (unless already changed)
            if ($purposeTravel->getUserDriver() === $this) {
                $purposeTravel->setUserDriver(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Travel[]
     */
    public function getIsPassenger(): Collection
    {
        return $this->is_passenger;
    }

    public function addIsPassenger(Travel $isPassenger): self
    {
        if (!$this->is_passenger->contains($isPassenger)) {
            $this->is_passenger[] = $isPassenger;
        }

        return $this;
    }

    public function removeIsPassenger(Travel $isPassenger): self
    {
        $this->is_passenger->removeElement($isPassenger);

        return $this;
    }

    public function getHavePreference(): ?Profile
    {
        return $this->have_preference;
    }

    public function setHavePreference(Profile $have_preference): self
    {
        $this->have_preference = $have_preference;

        return $this;
    }

//    /**
//     * @return Collection|Car[]
//     */
//    public function getCanDriveWith(): Collection
//    {
//        return $this->can_drive_with;
//    }
//
//    public function addCanDriveWith(Car $canDriveWith): self
//    {
//        if (!$this->can_drive_with->contains($canDriveWith)) {
//            $this->can_drive_with[] = $canDriveWith;
//            $canDriveWith->setUser($this);
//        }
//
//        return $this;
//    }
//
//    public function removeCanDriveWith(Car $canDriveWith): self
//    {
//        if ($this->can_drive_with->removeElement($canDriveWith)) {
//            // set the owning side to null (unless already changed)
//            if ($canDriveWith->getUser() === $this) {
//                $canDriveWith->setUser(null);
//            }
//        }
//
//        return $this;
//    }
}
