<?php

namespace App\Entity;

use App\Repository\ProfileRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProfileRepository::class)
 */
class Profile
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $smoke;

    /**
     * @ORM\Column(type="boolean")
     */
    private $animal;

    /**
     * @ORM\OneToMany(targetEntity=Travel::class, mappedBy="profile")
     */
    private $set_preference;

    public function __construct()
    {
        $this->set_preference = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSmoke(): ?bool
    {
        return $this->smoke;
    }

    public function setSmoke(bool $smoke): self
    {
        $this->smoke = $smoke;

        return $this;
    }

    public function getAnimal(): ?bool
    {
        return $this->animal;
    }

    public function setAnimal(bool $animal): self
    {
        $this->animal = $animal;

        return $this;
    }

    /**
     * @return Collection|Travel[]
     */
    public function getSetPreference(): Collection
    {
        return $this->set_preference;
    }

    public function addSetPreference(Travel $setPreference): self
    {
        if (!$this->set_preference->contains($setPreference)) {
            $this->set_preference[] = $setPreference;
            $setPreference->setProfile($this);
        }

        return $this;
    }

    public function removeSetPreference(Travel $setPreference): self
    {
        if ($this->set_preference->removeElement($setPreference)) {
            // set the owning side to null (unless already changed)
            if ($setPreference->getProfile() === $this) {
                $setPreference->setProfile(null);
            }
        }

        return $this;
    }
}
