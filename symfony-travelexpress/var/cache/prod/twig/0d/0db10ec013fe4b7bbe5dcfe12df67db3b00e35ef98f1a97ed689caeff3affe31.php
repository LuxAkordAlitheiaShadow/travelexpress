<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* search/new.html.twig */
class __TwigTemplate_7d40fb322f4e8f86897a8abd5b826bd631457e29e3bf8a140cf7d87dc9252ab9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "search/new.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "TravelExpress - Nouveau trajet";
    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    <div class=\"min-h-full flex items-center justify-center py-12 px-4\">
        <div class=\"max-w-md w-full space-y-8\">
            <div>
                <img class=\"mx-auto h-12 w-auto\" src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("logotravelexpress.png"), "html", null, true);
        echo "\" alt=\"Logo TravelExpress\">
                <h2 class=\"mt-6 text-center text-3xl font-bold text-gray-900\">
                    Créer un trajet
                </h2>
                <p class=\"mt-2 text-center text-sm text-gray-600\">
                    Ou
                    <a href=\"/search\" class=\"font-medium text-blue-600 hover:text-blue-500\">
                        recherchez en un maintenant
                    </a>
                </p>
            </div>
            <form class=\"mt-8 space-y-6\" action=\"#\" method=\"POST\">
                ";
        // line 21
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["new_travel_form"] ?? null), 'form_start');
        echo "
                <input type=\"hidden\" name=\"remember\" value=\"true\">
                <div class=\"rounded-md shadow-sm -space-y-px\">
                    <div class=\"mb-3 text-md font-semibold text-gray-900\">Ville de départ
                        ";
        // line 25
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["new_travel_form"] ?? null), "drive_from", [], "any", false, false, false, 25), 'widget');
        echo "
                    </div>
                    <div class=\"pb-3 text-md font-semibold text-gray-900\">Ville d'arrivée
                        ";
        // line 28
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["new_travel_form"] ?? null), "drive_to", [], "any", false, false, false, 28), 'widget');
        echo "
                    </div>
                    <div class=\"pb-3 text-md font-semibold text-gray-900\">Date de départ
                        ";
        // line 31
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["new_travel_form"] ?? null), "departure_datetime", [], "any", false, false, false, 31), 'widget');
        echo "
                    </div>
                    <div class=\"pb-3 text-md font-semibold text-gray-900\">Voiture utilisée
                        ";
        // line 34
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["new_travel_form"] ?? null), "car", [], "any", false, false, false, 34), 'widget');
        echo "
                    </div>
                    <div class=\"pb-3 text-md font-semibold text-gray-900\">Places disponibles
                        ";
        // line 37
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["new_travel_form"] ?? null), "seat_available", [], "any", false, false, false, 37), 'widget');
        echo "
                    </div>
                    <div class=\"pb-3 text-md font-semibold text-gray-900\">Conducteur
                        ";
        // line 40
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["new_travel_form"] ?? null), "user_driver", [], "any", false, false, false, 40), 'widget');
        echo "
                    </div>
                    <div class=\"text-md font-semibold text-gray-900\">Profile
                        ";
        // line 43
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["new_travel_form"] ?? null), "profile", [], "any", false, false, false, 43), 'widget');
        echo "
                    </div>
                </div>
                <div>
                    <button type=\"submit\" class=\"group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700\">
                        ";
        // line 48
        echo twig_escape_filter($this->env, ((array_key_exists("button_label", $context)) ? (_twig_default_filter(($context["button_label"] ?? null), "Ajouter")) : ("Ajouter")), "html", null, true);
        echo "
                    </button>
                </div>
                ";
        // line 51
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["new_travel_form"] ?? null), 'form_end');
        echo "
            </form>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "search/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  135 => 51,  129 => 48,  121 => 43,  115 => 40,  109 => 37,  103 => 34,  97 => 31,  91 => 28,  85 => 25,  78 => 21,  63 => 9,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "search/new.html.twig", "/home/lux/Documents/UQAC/Automne/Systemes_repartis/TP_2/travelexpress/symfony-travelexpress/templates/search/new.html.twig");
    }
}
