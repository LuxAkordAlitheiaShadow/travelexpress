<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* search/checkout.html.twig */
class __TwigTemplate_d35a1e3afc606d2492300a6a653b6a7718e225ebd20c7557a7c679f9e3c468cc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "search/checkout.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "TravelExpress - Trajet valide";
    }

    // line 6
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    Vous avez bien reserve votre trajet";
        // line 8
        echo "    Depart : ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["travel"] ?? null), "driveFrom", [], "any", false, false, false, 8), "cityName", [], "any", false, false, false, 8), "html", null, true);
        echo "
    Arrive : ";
        // line 9
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["travel"] ?? null), "driveTo", [], "any", false, false, false, 9), "cityName", [], "any", false, false, false, 9), "html", null, true);
        echo "
    Date : ";
        // line 10
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["travel"] ?? null), "departureDatetime", [], "any", false, false, false, 10), "d/m/Y"), "html", null, true);
        echo "
    Heure : ";
        // line 11
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["travel"] ?? null), "departureDatetime", [], "any", false, false, false, 11), "H:i"), "html", null, true);
        echo "
    Conducteur : ";
        // line 12
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["travel"] ?? null), "userDriver", [], "any", false, false, false, 12), "name", [], "any", false, false, false, 12), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["travel"] ?? null), "userDriver", [], "any", false, false, false, 12), "surname", [], "any", false, false, false, 12), "html", null, true);
        echo "
    Place reserve : ";
        // line 13
        echo twig_escape_filter($this->env, ($context["seat_wanted"] ?? null), "html", null, true);
        echo "
    Prix : 5 \$CAD
";
    }

    public function getTemplateName()
    {
        return "search/checkout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 13,  77 => 12,  73 => 11,  69 => 10,  65 => 9,  60 => 8,  58 => 7,  54 => 6,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "search/checkout.html.twig", "/home/lux/Documents/UQAC/Automne/Systemes_repartis/TP_2/travelexpress/symfony-travelexpress/templates/search/checkout.html.twig");
    }
}
