<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* user/new_user.html.twig */
class __TwigTemplate_dac3302aaf2dbe428c935e74eb39de0e55e7766f188e001054a4f9c2a026449c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "user/new_user.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "TravelExpress - Nouvel utilisateur";
    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    <div class=\"min-h-full flex items-center justify-center py-12 px-4\">
        <div class=\"max-w-md w-full space-y-8\">
            <div>
                <img class=\"mx-auto h-12 w-auto\" src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("logotravelexpress.png"), "html", null, true);
        echo "\" alt=\"Logo TravelExpress\">
                <h2 class=\"mt-6 text-center text-3xl font-bold text-gray-900\">
                    Créer un compte
                </h2>
                <p class=\"mt-2 text-center text-sm text-gray-600\">
                    Ou
                    <a href=\"/user/login\" class=\"font-medium text-blue-600 hover:text-blue-500\">
                        connectez-vous si vous en avez déjà un
                    </a>
                </p>
            </div>
            <form class=\"mt-8 space-y-6\" action=\"#\" method=\"POST\">
                ";
        // line 21
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["user_form"] ?? null), 'form_start');
        echo "
                <input type=\"hidden\" name=\"remember\" value=\"true\">
                <div class=\"rounded-md -space-y-px\">
                    <div class=\"mb-3 text-md font-semibold text-gray-900\">Nom
                        ";
        // line 25
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["user_form"] ?? null), "name", [], "any", false, false, false, 25), 'widget');
        echo "
                    </div>
                    <div class=\"mb-3 text-md font-semibold text-gray-900 pb-3\">Prénom
                        ";
        // line 28
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["user_form"] ?? null), "surname", [], "any", false, false, false, 28), 'widget');
        echo "
                    </div>
                    <div class=\"mb-3 text-md font-semibold text-gray-900 pb-3\">E-mail
                        ";
        // line 31
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["user_form"] ?? null), "mail", [], "any", false, false, false, 31), 'widget');
        echo "
                    </div>
                    <div class=\"text-md font-semibold text-gray-900 pb-3\">Mot de passe
                        ";
        // line 34
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, ($context["user_form"] ?? null), "hash_pwd", [], "any", false, false, false, 34), 'widget');
        echo "
                    </div>
                    <div class=\"text-md font-semibold text-gray-900\">Préférences
                        <br>
                        <div class=\"inline pr-5\"> Smoke ";
        // line 38
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user_form"] ?? null), "have_preference", [], "any", false, false, false, 38), "smoke", [], "any", false, false, false, 38), 'widget');
        echo "</div>
                        <div class=\"inline\"> Animal ";
        // line 39
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["user_form"] ?? null), "have_preference", [], "any", false, false, false, 39), "animal", [], "any", false, false, false, 39), 'widget');
        echo "</div>
                    </div>

                </div>
                <div>
                    <button type=\"submit\" class=\"group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700\">
                        ";
        // line 45
        echo twig_escape_filter($this->env, ((array_key_exists("button_label", $context)) ? (_twig_default_filter(($context["button_label"] ?? null), "Créer mon compte")) : ("Créer mon compte")), "html", null, true);
        echo "
                    </button>
                </div>
                ";
        // line 48
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["user_form"] ?? null), 'form_end');
        echo "
            </form>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "user/new_user.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  129 => 48,  123 => 45,  114 => 39,  110 => 38,  103 => 34,  97 => 31,  91 => 28,  85 => 25,  78 => 21,  63 => 9,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "user/new_user.html.twig", "/home/lux/Documents/UQAC/Automne/Systemes_repartis/TP_2/travelexpress/symfony-travelexpress/templates/user/new_user.html.twig");
    }
}
