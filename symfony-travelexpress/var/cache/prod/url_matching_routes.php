<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/' => [[['_route' => 'search', '_controller' => 'App\\Controller\\SearchController::index'], null, null, null, false, false, null]],
        '/travel/new' => [[['_route' => 'newTravel', '_controller' => 'App\\Controller\\SearchController::newTravel'], null, null, null, false, false, null]],
        '/travel' => [[['_route' => 'travel', '_controller' => 'App\\Controller\\SearchController::travel'], null, null, null, false, false, null]],
        '/user/login' => [[['_route' => 'login', '_controller' => 'App\\Controller\\UserController::login'], null, null, null, false, false, null]],
        '/user/logout' => [[['_route' => 'logout', '_controller' => 'App\\Controller\\UserController::logout'], null, null, null, false, false, null]],
        '/user/new' => [[['_route' => 'new_user', '_controller' => 'App\\Controller\\UserController::new_user'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/book/([^/]++)(*:21)'
                .'|/checkout/([^/]++)/([^/]++)(*:55)'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        21 => [[['_route' => 'book', '_controller' => 'App\\Controller\\SearchController::book'], ['id'], null, null, false, true, null]],
        55 => [
            [['_route' => 'checkout', '_controller' => 'App\\Controller\\SearchController::checkout'], ['id', 'seat_wanted'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
