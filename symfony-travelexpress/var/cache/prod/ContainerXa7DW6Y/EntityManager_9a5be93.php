<?php

namespace ContainerXa7DW6Y;

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolderc5895 = null;
    private $initializerd791c = null;
    private static $publicProperties2dbd4 = [
        
    ];
    public function getConnection()
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'getConnection', array(), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->getConnection();
    }
    public function getMetadataFactory()
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'getMetadataFactory', array(), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->getMetadataFactory();
    }
    public function getExpressionBuilder()
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'getExpressionBuilder', array(), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->getExpressionBuilder();
    }
    public function beginTransaction()
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'beginTransaction', array(), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->beginTransaction();
    }
    public function getCache()
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'getCache', array(), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->getCache();
    }
    public function transactional($func)
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'transactional', array('func' => $func), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->transactional($func);
    }
    public function wrapInTransaction(callable $func)
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'wrapInTransaction', array('func' => $func), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->wrapInTransaction($func);
    }
    public function commit()
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'commit', array(), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->commit();
    }
    public function rollback()
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'rollback', array(), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->rollback();
    }
    public function getClassMetadata($className)
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'getClassMetadata', array('className' => $className), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->getClassMetadata($className);
    }
    public function createQuery($dql = '')
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'createQuery', array('dql' => $dql), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->createQuery($dql);
    }
    public function createNamedQuery($name)
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'createNamedQuery', array('name' => $name), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->createNamedQuery($name);
    }
    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->createNativeQuery($sql, $rsm);
    }
    public function createNamedNativeQuery($name)
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->createNamedNativeQuery($name);
    }
    public function createQueryBuilder()
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'createQueryBuilder', array(), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->createQueryBuilder();
    }
    public function flush($entity = null)
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'flush', array('entity' => $entity), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->flush($entity);
    }
    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->find($className, $id, $lockMode, $lockVersion);
    }
    public function getReference($entityName, $id)
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->getReference($entityName, $id);
    }
    public function getPartialReference($entityName, $identifier)
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->getPartialReference($entityName, $identifier);
    }
    public function clear($entityName = null)
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'clear', array('entityName' => $entityName), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->clear($entityName);
    }
    public function close()
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'close', array(), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->close();
    }
    public function persist($entity)
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'persist', array('entity' => $entity), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->persist($entity);
    }
    public function remove($entity)
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'remove', array('entity' => $entity), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->remove($entity);
    }
    public function refresh($entity)
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'refresh', array('entity' => $entity), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->refresh($entity);
    }
    public function detach($entity)
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'detach', array('entity' => $entity), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->detach($entity);
    }
    public function merge($entity)
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'merge', array('entity' => $entity), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->merge($entity);
    }
    public function copy($entity, $deep = false)
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->copy($entity, $deep);
    }
    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->lock($entity, $lockMode, $lockVersion);
    }
    public function getRepository($entityName)
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'getRepository', array('entityName' => $entityName), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->getRepository($entityName);
    }
    public function contains($entity)
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'contains', array('entity' => $entity), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->contains($entity);
    }
    public function getEventManager()
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'getEventManager', array(), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->getEventManager();
    }
    public function getConfiguration()
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'getConfiguration', array(), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->getConfiguration();
    }
    public function isOpen()
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'isOpen', array(), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->isOpen();
    }
    public function getUnitOfWork()
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'getUnitOfWork', array(), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->getUnitOfWork();
    }
    public function getHydrator($hydrationMode)
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->getHydrator($hydrationMode);
    }
    public function newHydrator($hydrationMode)
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->newHydrator($hydrationMode);
    }
    public function getProxyFactory()
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'getProxyFactory', array(), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->getProxyFactory();
    }
    public function initializeObject($obj)
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'initializeObject', array('obj' => $obj), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->initializeObject($obj);
    }
    public function getFilters()
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'getFilters', array(), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->getFilters();
    }
    public function isFiltersStateClean()
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'isFiltersStateClean', array(), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->isFiltersStateClean();
    }
    public function hasFilters()
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'hasFilters', array(), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return $this->valueHolderc5895->hasFilters();
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);
        $instance->initializerd791c = $initializer;
        return $instance;
    }
    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;
        if (! $this->valueHolderc5895) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolderc5895 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
        }
        $this->valueHolderc5895->__construct($conn, $config, $eventManager);
    }
    public function & __get($name)
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, '__get', ['name' => $name], $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        if (isset(self::$publicProperties2dbd4[$name])) {
            return $this->valueHolderc5895->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderc5895;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolderc5895;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, '__set', array('name' => $name, 'value' => $value), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderc5895;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolderc5895;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, '__isset', array('name' => $name), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderc5895;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolderc5895;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, '__unset', array('name' => $name), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderc5895;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolderc5895;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, '__clone', array(), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        $this->valueHolderc5895 = clone $this->valueHolderc5895;
    }
    public function __sleep()
    {
        $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, '__sleep', array(), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
        return array('valueHolderc5895');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializerd791c = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializerd791c;
    }
    public function initializeProxy() : bool
    {
        return $this->initializerd791c && ($this->initializerd791c->__invoke($valueHolderc5895, $this, 'initializeProxy', array(), $this->initializerd791c) || 1) && $this->valueHolderc5895 = $valueHolderc5895;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolderc5895;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolderc5895;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
