<?php

namespace ContainerNzfKlCS;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder4a1b3 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer09405 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicPropertiesb084f = [
        
    ];

    public function getConnection()
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'getConnection', array(), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'getMetadataFactory', array(), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'getExpressionBuilder', array(), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'beginTransaction', array(), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'getCache', array(), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->getCache();
    }

    public function transactional($func)
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'transactional', array('func' => $func), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'wrapInTransaction', array('func' => $func), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'commit', array(), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->commit();
    }

    public function rollback()
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'rollback', array(), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'getClassMetadata', array('className' => $className), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'createQuery', array('dql' => $dql), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'createNamedQuery', array('name' => $name), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'createQueryBuilder', array(), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'flush', array('entity' => $entity), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'clear', array('entityName' => $entityName), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->clear($entityName);
    }

    public function close()
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'close', array(), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->close();
    }

    public function persist($entity)
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'persist', array('entity' => $entity), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'remove', array('entity' => $entity), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'refresh', array('entity' => $entity), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'detach', array('entity' => $entity), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'merge', array('entity' => $entity), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'getRepository', array('entityName' => $entityName), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'contains', array('entity' => $entity), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'getEventManager', array(), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'getConfiguration', array(), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'isOpen', array(), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'getUnitOfWork', array(), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'getProxyFactory', array(), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'initializeObject', array('obj' => $obj), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'getFilters', array(), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'isFiltersStateClean', array(), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'hasFilters', array(), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return $this->valueHolder4a1b3->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer09405 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder4a1b3) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder4a1b3 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder4a1b3->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, '__get', ['name' => $name], $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        if (isset(self::$publicPropertiesb084f[$name])) {
            return $this->valueHolder4a1b3->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder4a1b3;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder4a1b3;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder4a1b3;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder4a1b3;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, '__isset', array('name' => $name), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder4a1b3;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder4a1b3;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, '__unset', array('name' => $name), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder4a1b3;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder4a1b3;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, '__clone', array(), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        $this->valueHolder4a1b3 = clone $this->valueHolder4a1b3;
    }

    public function __sleep()
    {
        $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, '__sleep', array(), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;

        return array('valueHolder4a1b3');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer09405 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer09405;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer09405 && ($this->initializer09405->__invoke($valueHolder4a1b3, $this, 'initializeProxy', array(), $this->initializer09405) || 1) && $this->valueHolder4a1b3 = $valueHolder4a1b3;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder4a1b3;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder4a1b3;
    }


}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
