<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* user/login.html.twig */
class __TwigTemplate_e13ef870f11a803b9af56cc3357c33d9347645c1120ab519db3da05ce0d366f4 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "user/login.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "user/login.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "user/login.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "TravelExpress - Connexion";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"min-h-full flex items-center justify-center py-12 px-4\">
        <div class=\"max-w-md w-full space-y-8\">
            <div>
                <img class=\"mx-auto h-12 w-auto\" src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("logotravelexpress.png"), "html", null, true);
        echo "\" alt=\"Logo TravelExpress\">
                <h2 class=\"mt-6 text-center text-3xl font-bold text-gray-900\">
                    Connectez-vous à votre compte
                </h2>
                <p class=\"mt-2 text-center text-sm text-gray-600\">
                    Ou
                    <a href=\"/user/new\" class=\"font-medium text-blue-600 hover:text-blue-500\">
                        créez-vous un compte maintenant
                    </a>
                </p>
            </div>
            <form class=\"mt-8 space-y-6\" action=\"#\" method=\"POST\">
                ";
        // line 21
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["login_form"]) || array_key_exists("login_form", $context) ? $context["login_form"] : (function () { throw new RuntimeError('Variable "login_form" does not exist.', 21, $this->source); })()), 'form_start');
        echo "
                <input type=\"hidden\" name=\"remember\" value=\"true\">
                <div class=\"rounded-md -space-y-px\">
                    <div class=\"mb-3 text-md font-semibold text-gray-900\">E-mail
                        ";
        // line 25
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["login_form"]) || array_key_exists("login_form", $context) ? $context["login_form"] : (function () { throw new RuntimeError('Variable "login_form" does not exist.', 25, $this->source); })()), "mail", [], "any", false, false, false, 25), 'widget');
        echo "
                    </div>
                    <div class=\"text-md font-semibold text-gray-900\">Mot de passe
                        ";
        // line 28
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["login_form"]) || array_key_exists("login_form", $context) ? $context["login_form"] : (function () { throw new RuntimeError('Variable "login_form" does not exist.', 28, $this->source); })()), "hash_pwd", [], "any", false, false, false, 28), 'widget');
        echo "
                    </div>
                </div>
                ";
        // line 31
        if (((isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new RuntimeError('Variable "error" does not exist.', 31, $this->source); })()) == 1)) {
            // line 32
            echo "                <div class=\"text-red-600 font-base font-normal mb-2\">
                    <p>
                        <svg xmlns=\"http://www.w3.org/2000/svg\" class=\"inline h-5 w-5\" fill=\"none\" viewBox=\"0 0 24 24\" stroke=\"currentColor\"><path stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-width=\"2\" d=\"M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z\" /></svg>
                        Votre identifiant ou mot de passe est incorrect.
                    </p>
                </div>
                ";
        }
        // line 39
        echo "                <div>
                    <button type=\"submit\" class=\"group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700\">
                        ";
        // line 41
        echo twig_escape_filter($this->env, ((array_key_exists("button_label", $context)) ? (_twig_default_filter((isset($context["button_label"]) || array_key_exists("button_label", $context) ? $context["button_label"] : (function () { throw new RuntimeError('Variable "button_label" does not exist.', 41, $this->source); })()), "Se connecter")) : ("Se connecter")), "html", null, true);
        echo "
                    </button>
                </div>
                ";
        // line 44
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["login_form"]) || array_key_exists("login_form", $context) ? $context["login_form"] : (function () { throw new RuntimeError('Variable "login_form" does not exist.', 44, $this->source); })()), 'form_end');
        echo "
            </form>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "user/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  148 => 44,  142 => 41,  138 => 39,  129 => 32,  127 => 31,  121 => 28,  115 => 25,  108 => 21,  93 => 9,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}TravelExpress - Connexion{% endblock %}

{% block body %}
    <div class=\"min-h-full flex items-center justify-center py-12 px-4\">
        <div class=\"max-w-md w-full space-y-8\">
            <div>
                <img class=\"mx-auto h-12 w-auto\" src=\"{{ asset('logotravelexpress.png') }}\" alt=\"Logo TravelExpress\">
                <h2 class=\"mt-6 text-center text-3xl font-bold text-gray-900\">
                    Connectez-vous à votre compte
                </h2>
                <p class=\"mt-2 text-center text-sm text-gray-600\">
                    Ou
                    <a href=\"/user/new\" class=\"font-medium text-blue-600 hover:text-blue-500\">
                        créez-vous un compte maintenant
                    </a>
                </p>
            </div>
            <form class=\"mt-8 space-y-6\" action=\"#\" method=\"POST\">
                {{ form_start(login_form) }}
                <input type=\"hidden\" name=\"remember\" value=\"true\">
                <div class=\"rounded-md -space-y-px\">
                    <div class=\"mb-3 text-md font-semibold text-gray-900\">E-mail
                        {{ form_widget(login_form.mail) }}
                    </div>
                    <div class=\"text-md font-semibold text-gray-900\">Mot de passe
                        {{ form_widget(login_form.hash_pwd) }}
                    </div>
                </div>
                {% if error == 1 %}
                <div class=\"text-red-600 font-base font-normal mb-2\">
                    <p>
                        <svg xmlns=\"http://www.w3.org/2000/svg\" class=\"inline h-5 w-5\" fill=\"none\" viewBox=\"0 0 24 24\" stroke=\"currentColor\"><path stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-width=\"2\" d=\"M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z\" /></svg>
                        Votre identifiant ou mot de passe est incorrect.
                    </p>
                </div>
                {% endif %}
                <div>
                    <button type=\"submit\" class=\"group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700\">
                        {{ button_label|default('Se connecter') }}
                    </button>
                </div>
                {{ form_end(login_form) }}
            </form>
        </div>
    </div>
{% endblock %}", "user/login.html.twig", "/var/www/symfony_travelexpress/templates/user/login.html.twig");
    }
}
