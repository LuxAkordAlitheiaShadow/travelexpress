<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* search/new.html.twig */
class __TwigTemplate_749b07612dd875605831eb3cce4b556d75c1257b7439130bbc2d8b6b13be6f95 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "search/new.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "search/new.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "search/new.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "TravelExpress - Nouveau trajet";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"min-h-full flex items-center justify-center py-12 px-4\">
        <div class=\"max-w-md w-full space-y-8\">
            <div>
                <img class=\"mx-auto h-12 w-auto\" src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("logotravelexpress.png"), "html", null, true);
        echo "\" alt=\"Logo TravelExpress\">
                <h2 class=\"mt-6 text-center text-3xl font-bold text-gray-900\">
                    Créer un trajet
                </h2>
                <p class=\"mt-2 text-center text-sm text-gray-600\">
                    Ou
                    <a href=\"/search\" class=\"font-medium text-blue-600 hover:text-blue-500\">
                        recherchez en un maintenant
                    </a>
                </p>
            </div>
            <form class=\"mt-8 space-y-6\" action=\"#\" method=\"POST\">
                ";
        // line 21
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["new_travel_form"]) || array_key_exists("new_travel_form", $context) ? $context["new_travel_form"] : (function () { throw new RuntimeError('Variable "new_travel_form" does not exist.', 21, $this->source); })()), 'form_start');
        echo "
                <input type=\"hidden\" name=\"remember\" value=\"true\">
                <div>
                    <div class=\"mb-3 text-md font-semibold text-gray-900\">Ville de départ
                        ";
        // line 25
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["new_travel_form"]) || array_key_exists("new_travel_form", $context) ? $context["new_travel_form"] : (function () { throw new RuntimeError('Variable "new_travel_form" does not exist.', 25, $this->source); })()), "drive_from", [], "any", false, false, false, 25), 'widget');
        echo "
                    </div>
                    <div class=\"pb-3 text-md font-semibold text-gray-900\">Ville d'arrivée
                        ";
        // line 28
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["new_travel_form"]) || array_key_exists("new_travel_form", $context) ? $context["new_travel_form"] : (function () { throw new RuntimeError('Variable "new_travel_form" does not exist.', 28, $this->source); })()), "drive_to", [], "any", false, false, false, 28), 'widget');
        echo "
                    </div>
                    <div class=\"pb-3 text-md font-semibold text-gray-900\">Date de départ
                        ";
        // line 31
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["new_travel_form"]) || array_key_exists("new_travel_form", $context) ? $context["new_travel_form"] : (function () { throw new RuntimeError('Variable "new_travel_form" does not exist.', 31, $this->source); })()), "departure_datetime", [], "any", false, false, false, 31), 'widget');
        echo "
                    </div>
";
        // line 36
        echo "                    <div class=\"pb-3 text-md font-semibold text-gray-900\">Places disponibles
                        ";
        // line 37
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["new_travel_form"]) || array_key_exists("new_travel_form", $context) ? $context["new_travel_form"] : (function () { throw new RuntimeError('Variable "new_travel_form" does not exist.', 37, $this->source); })()), "seat_available", [], "any", false, false, false, 37), 'widget');
        echo "
                    </div>
                    ";
        // line 45
        echo "                </div>
                <div>
                    <button type=\"submit\" class=\"group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700\">
                        ";
        // line 48
        echo twig_escape_filter($this->env, ((array_key_exists("button_label", $context)) ? (_twig_default_filter((isset($context["button_label"]) || array_key_exists("button_label", $context) ? $context["button_label"] : (function () { throw new RuntimeError('Variable "button_label" does not exist.', 48, $this->source); })()), "Ajouter")) : ("Ajouter")), "html", null, true);
        echo "
                    </button>
                </div>
                ";
        // line 51
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["new_travel_form"]) || array_key_exists("new_travel_form", $context) ? $context["new_travel_form"] : (function () { throw new RuntimeError('Variable "new_travel_form" does not exist.', 51, $this->source); })()), 'form_end');
        echo "
            </form>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "search/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 51,  145 => 48,  140 => 45,  135 => 37,  132 => 36,  127 => 31,  121 => 28,  115 => 25,  108 => 21,  93 => 9,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}TravelExpress - Nouveau trajet{% endblock %}

{% block body %}
    <div class=\"min-h-full flex items-center justify-center py-12 px-4\">
        <div class=\"max-w-md w-full space-y-8\">
            <div>
                <img class=\"mx-auto h-12 w-auto\" src=\"{{ asset('logotravelexpress.png') }}\" alt=\"Logo TravelExpress\">
                <h2 class=\"mt-6 text-center text-3xl font-bold text-gray-900\">
                    Créer un trajet
                </h2>
                <p class=\"mt-2 text-center text-sm text-gray-600\">
                    Ou
                    <a href=\"/search\" class=\"font-medium text-blue-600 hover:text-blue-500\">
                        recherchez en un maintenant
                    </a>
                </p>
            </div>
            <form class=\"mt-8 space-y-6\" action=\"#\" method=\"POST\">
                {{ form_start(new_travel_form) }}
                <input type=\"hidden\" name=\"remember\" value=\"true\">
                <div>
                    <div class=\"mb-3 text-md font-semibold text-gray-900\">Ville de départ
                        {{ form_widget(new_travel_form.drive_from) }}
                    </div>
                    <div class=\"pb-3 text-md font-semibold text-gray-900\">Ville d'arrivée
                        {{ form_widget(new_travel_form.drive_to) }}
                    </div>
                    <div class=\"pb-3 text-md font-semibold text-gray-900\">Date de départ
                        {{ form_widget(new_travel_form.departure_datetime) }}
                    </div>
{#                    <div class=\"pb-3 text-md font-semibold text-gray-900\">Voiture utilisée#}
{#                        {{ form_widget(new_travel_form.car) }}#}
{#                    </div>#}
                    <div class=\"pb-3 text-md font-semibold text-gray-900\">Places disponibles
                        {{ form_widget(new_travel_form.seat_available) }}
                    </div>
                    {#                    <div class=\"pb-3 text-md font-semibold text-gray-900\">Conducteur#}
{#                        {{ form_widget(new_travel_form.user_driver) }}#}
{#                    </div>#}
{#                    <div class=\"text-md font-semibold text-gray-900\">Profile#}
{#                        {{ form_widget(new_travel_form.profile) }}#}
{#                    </div>#}
                </div>
                <div>
                    <button type=\"submit\" class=\"group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700\">
                        {{ button_label|default('Ajouter') }}
                    </button>
                </div>
                {{ form_end(new_travel_form) }}
            </form>
        </div>
    </div>
{% endblock %}", "search/new.html.twig", "/var/www/symfony_travelexpress/templates/search/new.html.twig");
    }
}
