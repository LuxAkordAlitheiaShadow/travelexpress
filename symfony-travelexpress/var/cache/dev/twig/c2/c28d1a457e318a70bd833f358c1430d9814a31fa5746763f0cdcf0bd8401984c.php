<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* user/new_user.html.twig */
class __TwigTemplate_47a2e2bc605c11d0c2af57ad02ff7457c4f96d3bb42efbeb205ca5d3f7ebf5ea extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "user/new_user.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "user/new_user.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "user/new_user.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "TravelExpress - Nouvel utilisateur";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"min-h-full flex items-center justify-center py-12 px-4\">
        <div class=\"max-w-md w-full space-y-8\">
            <div>
                <img class=\"mx-auto h-12 w-auto\" src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("logotravelexpress.png"), "html", null, true);
        echo "\" alt=\"Logo TravelExpress\">
                <h2 class=\"mt-6 text-center text-3xl font-bold text-gray-900\">
                    Créer un compte
                </h2>
                <p class=\"mt-2 text-center text-sm text-gray-600\">
                    Ou
                    <a href=\"/user/login\" class=\"font-medium text-blue-600 hover:text-blue-500\">
                        connectez-vous si vous en avez déjà un
                    </a>
                </p>
            </div>
            <form class=\"mt-8 space-y-6\" action=\"#\" method=\"POST\">
                ";
        // line 21
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["user_form"]) || array_key_exists("user_form", $context) ? $context["user_form"] : (function () { throw new RuntimeError('Variable "user_form" does not exist.', 21, $this->source); })()), 'form_start');
        echo "
                <input type=\"hidden\" name=\"remember\" value=\"true\">
                <div class=\"rounded-md -space-y-px\">
                    <div class=\"mb-3 text-md font-semibold text-gray-900\">Nom
                        ";
        // line 25
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["user_form"]) || array_key_exists("user_form", $context) ? $context["user_form"] : (function () { throw new RuntimeError('Variable "user_form" does not exist.', 25, $this->source); })()), "name", [], "any", false, false, false, 25), 'widget');
        echo "
                    </div>
                    <div class=\"mb-3 text-md font-semibold text-gray-900 pb-3\">Prénom
                        ";
        // line 28
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["user_form"]) || array_key_exists("user_form", $context) ? $context["user_form"] : (function () { throw new RuntimeError('Variable "user_form" does not exist.', 28, $this->source); })()), "surname", [], "any", false, false, false, 28), 'widget');
        echo "
                    </div>
                    <div class=\"mb-3 text-md font-semibold text-gray-900 pb-3\">E-mail
                        ";
        // line 31
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["user_form"]) || array_key_exists("user_form", $context) ? $context["user_form"] : (function () { throw new RuntimeError('Variable "user_form" does not exist.', 31, $this->source); })()), "mail", [], "any", false, false, false, 31), 'widget');
        echo "
                    </div>
                    <div class=\"text-md font-semibold text-gray-900 pb-3\">Mot de passe
                        ";
        // line 34
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["user_form"]) || array_key_exists("user_form", $context) ? $context["user_form"] : (function () { throw new RuntimeError('Variable "user_form" does not exist.', 34, $this->source); })()), "hash_pwd", [], "any", false, false, false, 34), 'widget');
        echo "
                    </div>
                    <div class=\"text-md font-semibold text-gray-900\">Préférences
                        <br>
                        <div class=\"inline pr-5\"> Smoke ";
        // line 38
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user_form"]) || array_key_exists("user_form", $context) ? $context["user_form"] : (function () { throw new RuntimeError('Variable "user_form" does not exist.', 38, $this->source); })()), "have_preference", [], "any", false, false, false, 38), "smoke", [], "any", false, false, false, 38), 'widget');
        echo "</div>
                        <div class=\"inline\"> Animal ";
        // line 39
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["user_form"]) || array_key_exists("user_form", $context) ? $context["user_form"] : (function () { throw new RuntimeError('Variable "user_form" does not exist.', 39, $this->source); })()), "have_preference", [], "any", false, false, false, 39), "animal", [], "any", false, false, false, 39), 'widget');
        echo "</div>
                    </div>

                </div>
                <div>
                    <button type=\"submit\" class=\"group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700\">
                        ";
        // line 45
        echo twig_escape_filter($this->env, ((array_key_exists("button_label", $context)) ? (_twig_default_filter((isset($context["button_label"]) || array_key_exists("button_label", $context) ? $context["button_label"] : (function () { throw new RuntimeError('Variable "button_label" does not exist.', 45, $this->source); })()), "Créer mon compte")) : ("Créer mon compte")), "html", null, true);
        echo "
                    </button>
                </div>
                ";
        // line 48
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["user_form"]) || array_key_exists("user_form", $context) ? $context["user_form"] : (function () { throw new RuntimeError('Variable "user_form" does not exist.', 48, $this->source); })()), 'form_end');
        echo "
            </form>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "user/new_user.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  159 => 48,  153 => 45,  144 => 39,  140 => 38,  133 => 34,  127 => 31,  121 => 28,  115 => 25,  108 => 21,  93 => 9,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}TravelExpress - Nouvel utilisateur{% endblock %}

{% block body %}
    <div class=\"min-h-full flex items-center justify-center py-12 px-4\">
        <div class=\"max-w-md w-full space-y-8\">
            <div>
                <img class=\"mx-auto h-12 w-auto\" src=\"{{ asset('logotravelexpress.png') }}\" alt=\"Logo TravelExpress\">
                <h2 class=\"mt-6 text-center text-3xl font-bold text-gray-900\">
                    Créer un compte
                </h2>
                <p class=\"mt-2 text-center text-sm text-gray-600\">
                    Ou
                    <a href=\"/user/login\" class=\"font-medium text-blue-600 hover:text-blue-500\">
                        connectez-vous si vous en avez déjà un
                    </a>
                </p>
            </div>
            <form class=\"mt-8 space-y-6\" action=\"#\" method=\"POST\">
                {{ form_start(user_form) }}
                <input type=\"hidden\" name=\"remember\" value=\"true\">
                <div class=\"rounded-md -space-y-px\">
                    <div class=\"mb-3 text-md font-semibold text-gray-900\">Nom
                        {{ form_widget(user_form.name) }}
                    </div>
                    <div class=\"mb-3 text-md font-semibold text-gray-900 pb-3\">Prénom
                        {{ form_widget(user_form.surname) }}
                    </div>
                    <div class=\"mb-3 text-md font-semibold text-gray-900 pb-3\">E-mail
                        {{ form_widget(user_form.mail) }}
                    </div>
                    <div class=\"text-md font-semibold text-gray-900 pb-3\">Mot de passe
                        {{ form_widget(user_form.hash_pwd) }}
                    </div>
                    <div class=\"text-md font-semibold text-gray-900\">Préférences
                        <br>
                        <div class=\"inline pr-5\"> Smoke {{ form_widget(user_form.have_preference.smoke) }}</div>
                        <div class=\"inline\"> Animal {{ form_widget(user_form.have_preference.animal) }}</div>
                    </div>

                </div>
                <div>
                    <button type=\"submit\" class=\"group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700\">
                        {{ button_label|default('Créer mon compte') }}
                    </button>
                </div>
                {{ form_end(user_form) }}
            </form>
        </div>
    </div>
{% endblock %}", "user/new_user.html.twig", "/var/www/symfony_travelexpress/templates/user/new_user.html.twig");
    }
}
