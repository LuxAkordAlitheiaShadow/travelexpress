<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base.html.twig */
class __TwigTemplate_3159dfa3e4f9e027c97bdf782e4593c180d4836dd56c6ba07d317df347ba07cd extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'javascripts' => [$this, 'block_javascripts'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 8
        echo "        ";
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 12
        echo "
        ";
        // line 13
        $this->displayBlock('javascripts', $context, $blocks);
        // line 16
        echo "    </head>
    <body class=\"h-full\">
        <header>
            <nav class=\"bg-blue-800\">
                <div class=\"max-w-7xl mx-auto px-2 sm:px-6 lg:px-8\">
                    <div class=\"relative flex items-center justify-between h-16\">
                        <div class=\"absolute inset-y-0 left-0 flex items-center sm:hidden\">
                            <!-- Mobile menu button-->
                            <button type=\"button\" class=\"inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white\" aria-controls=\"mobile-menu\" aria-expanded=\"false\">
                                <span class=\"sr-only\">Ouvrir le menu principal</span>
                                <!--
                                  Icon when menu is closed.
                                  Heroicon name: outline/menu
                                  Menu open: \"hidden\", Menu closed: \"block\"
                                -->
                                <svg class=\"block h-6 w-6\" xmlns=\"http://www.w3.org/2000/svg\" fill=\"none\" viewBox=\"0 0 24 24\" stroke=\"currentColor\" aria-hidden=\"true\">
                                    <path stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-width=\"2\" d=\"M4 6h16M4 12h16M4 18h16\" />
                                </svg>
                                <!--
                                  Icon when menu is open.
                                  Heroicon name: outline/x
                                  Menu open: \"block\", Menu closed: \"hidden\"
                                -->
                                <svg class=\"hidden h-6 w-6\" xmlns=\"http://www.w3.org/2000/svg\" fill=\"none\" viewBox=\"0 0 24 24\" stroke=\"currentColor\" aria-hidden=\"true\">
                                    <path stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-width=\"2\" d=\"M6 18L18 6M6 6l12 12\" />
                                </svg>
                            </button>
                        </div>
                        <div class=\"flex-1 flex items-center justify-center sm:items-stretch sm:justify-start\">
                            <div class=\"flex-shrink-0 flex items-center\">
                                <img class=\"block h-8 w-auto\" src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("logotravelexpresswhite.png"), "html", null, true);
        echo "\" alt=\"Workflow\">
                            </div>
                            <div class=\"hidden sm:block sm:ml-6\">
                                <div class=\"flex space-x-4\">
                                    <a href=\"/\" class=\"text-gray-300 hover:bg-gray-900 hover:text-white px-3 py-2 rounded-md text-sm font-medium\">Recherche</a>
                                    <a href=\"/travel\" class=\"text-gray-300 hover:bg-gray-900 hover:text-white px-3 py-2 rounded-md text-sm font-medium\">Trajets</a>
                                </div>
                            </div>
                        </div>
                        <div class=\"absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0\">
                            ";
        // line 56
        if ((isset($context["user_id"]) || array_key_exists("user_id", $context) ? $context["user_id"] : (function () { throw new RuntimeError('Variable "user_id" does not exist.', 56, $this->source); })())) {
            // line 57
            echo "                                <a href=\"/user\" class=\"bg-gray-900 hover:bg-gray-800 px-3 py-2 mr-0.5 rounded-l-md text-sm font-medium\" aria-current=\"page\">
                                    <span class=\"text-white text-sm font-semibold\">
                                        <svg xmlns=\"http://www.w3.org/2000/svg\" class=\"h-5 w-5 mr-1 inline\" fill=\"none\" viewBox=\"0 0 24 24\" stroke=\"currentColor\">
                                            <path stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-width=\"2\" d=\"M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z\" />
                                        </svg>
                                        Mon compte
                                    </span>
                                </a>
                                <a href=\"/user/logout\" class=\"bg-gray-900 hover:bg-red-600 pl-3 pr-2 py-2 rounded-r-md text-sm font-medium\" aria-current=\"page\">
                                <span class=\"text-white\">
                                    <svg xmlns=\"http://www.w3.org/2000/svg\" class=\"h-5 w-5 inline\" fill=\"none\" viewBox=\"0 0 24 24\" stroke=\"currentColor\">
                                        <path stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-width=\"2\" d=\"M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1\" />
                                    </svg><span class=\"hidden\">.</span>
                                </span>
                                </a>
                            ";
        } else {
            // line 73
            echo "                                <a href=\"/user/login\" class=\"bg-gray-900 hover:bg-gray-800 px-3 py-2 mr-0.5 rounded-l-md text-sm font-medium\" aria-current=\"page\">
                                    <span class=\"text-white text-sm font-semibold\">
                                        <svg xmlns=\"http://www.w3.org/2000/svg\" class=\"h-5 w-5 mr-1 inline\" fill=\"none\" viewBox=\"0 0 24 24\" stroke=\"currentColor\">
                                            <path stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-width=\"2\" d=\"M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z\" />
                                        </svg>
                                        Se connecter
                                    </span>
                                </a>
                                <a href=\"/user/new\" class=\"bg-gray-900 hover:bg-green-600 pl-2 pr-1 py-2 rounded-r-md text-sm font-medium\" aria-current=\"page\">
                                <span class=\"text-white\">
                                    <svg xmlns=\"http://www.w3.org/2000/svg\" class=\"h-5 w-5 mr-1 inline\" fill=\"none\" viewBox=\"0 0 24 24\" stroke=\"currentColor\">
                                        <path stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-width=\"2\" d=\"M12 6v6m0 0v6m0-6h6m-6 0H6\" />
                                    </svg><span class=\"hidden\">.</span>
                                </span>
                                </a>
                            ";
        }
        // line 89
        echo "                        </div>
                    </div>
                </div>
                <!-- Mobile menu, show/hide based on menu state. -->
                <div class=\"sm:hidden\" id=\"mobile-menu\">
                    <div class=\"px-2 pt-2 pb-3 space-y-1\">
                        <a href=\"/search\" class=\"text-gray-300 hover:bg-gray-900 hover:text-white block px-3 py-2 rounded-md text-base font-medium\">Recherche</a>
                        <a href=\"/travel\" class=\"text-gray-300 hover:bg-gray-900 hover:text-white block px-3 py-2 rounded-md text-base font-medium\">Trajets</a>
                    </div>
                </div>
            </nav>
        </header>
        ";
        // line 101
        $this->displayBlock('body', $context, $blocks);
        // line 102
        echo "    </body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "TravelExpress";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 8
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 9
        echo "            ";
        // line 10
        echo "            <link href=\"https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css\" rel=\"stylesheet\">
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 13
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 14
        echo "            ";
        // line 15
        echo "        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 101
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  240 => 101,  230 => 15,  228 => 14,  218 => 13,  207 => 10,  205 => 9,  195 => 8,  176 => 5,  164 => 102,  162 => 101,  148 => 89,  130 => 73,  112 => 57,  110 => 56,  97 => 46,  65 => 16,  63 => 13,  60 => 12,  57 => 8,  53 => 5,  47 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <title>{% block title %}TravelExpress{% endblock %}</title>
        {# Run `composer require symfony/webpack-encore-bundle`
           and uncomment the following Encore helpers to start using Symfony UX #}
        {% block stylesheets %}
            {#{{ encore_entry_link_tags('app') }}#}
            <link href=\"https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css\" rel=\"stylesheet\">
        {% endblock %}

        {% block javascripts %}
            {#{{ encore_entry_script_tags('app') }}#}
        {% endblock %}
    </head>
    <body class=\"h-full\">
        <header>
            <nav class=\"bg-blue-800\">
                <div class=\"max-w-7xl mx-auto px-2 sm:px-6 lg:px-8\">
                    <div class=\"relative flex items-center justify-between h-16\">
                        <div class=\"absolute inset-y-0 left-0 flex items-center sm:hidden\">
                            <!-- Mobile menu button-->
                            <button type=\"button\" class=\"inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white\" aria-controls=\"mobile-menu\" aria-expanded=\"false\">
                                <span class=\"sr-only\">Ouvrir le menu principal</span>
                                <!--
                                  Icon when menu is closed.
                                  Heroicon name: outline/menu
                                  Menu open: \"hidden\", Menu closed: \"block\"
                                -->
                                <svg class=\"block h-6 w-6\" xmlns=\"http://www.w3.org/2000/svg\" fill=\"none\" viewBox=\"0 0 24 24\" stroke=\"currentColor\" aria-hidden=\"true\">
                                    <path stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-width=\"2\" d=\"M4 6h16M4 12h16M4 18h16\" />
                                </svg>
                                <!--
                                  Icon when menu is open.
                                  Heroicon name: outline/x
                                  Menu open: \"block\", Menu closed: \"hidden\"
                                -->
                                <svg class=\"hidden h-6 w-6\" xmlns=\"http://www.w3.org/2000/svg\" fill=\"none\" viewBox=\"0 0 24 24\" stroke=\"currentColor\" aria-hidden=\"true\">
                                    <path stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-width=\"2\" d=\"M6 18L18 6M6 6l12 12\" />
                                </svg>
                            </button>
                        </div>
                        <div class=\"flex-1 flex items-center justify-center sm:items-stretch sm:justify-start\">
                            <div class=\"flex-shrink-0 flex items-center\">
                                <img class=\"block h-8 w-auto\" src=\"{{ asset('logotravelexpresswhite.png') }}\" alt=\"Workflow\">
                            </div>
                            <div class=\"hidden sm:block sm:ml-6\">
                                <div class=\"flex space-x-4\">
                                    <a href=\"/\" class=\"text-gray-300 hover:bg-gray-900 hover:text-white px-3 py-2 rounded-md text-sm font-medium\">Recherche</a>
                                    <a href=\"/travel\" class=\"text-gray-300 hover:bg-gray-900 hover:text-white px-3 py-2 rounded-md text-sm font-medium\">Trajets</a>
                                </div>
                            </div>
                        </div>
                        <div class=\"absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0\">
                            {%  if user_id %}
                                <a href=\"/user\" class=\"bg-gray-900 hover:bg-gray-800 px-3 py-2 mr-0.5 rounded-l-md text-sm font-medium\" aria-current=\"page\">
                                    <span class=\"text-white text-sm font-semibold\">
                                        <svg xmlns=\"http://www.w3.org/2000/svg\" class=\"h-5 w-5 mr-1 inline\" fill=\"none\" viewBox=\"0 0 24 24\" stroke=\"currentColor\">
                                            <path stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-width=\"2\" d=\"M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z\" />
                                        </svg>
                                        Mon compte
                                    </span>
                                </a>
                                <a href=\"/user/logout\" class=\"bg-gray-900 hover:bg-red-600 pl-3 pr-2 py-2 rounded-r-md text-sm font-medium\" aria-current=\"page\">
                                <span class=\"text-white\">
                                    <svg xmlns=\"http://www.w3.org/2000/svg\" class=\"h-5 w-5 inline\" fill=\"none\" viewBox=\"0 0 24 24\" stroke=\"currentColor\">
                                        <path stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-width=\"2\" d=\"M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1\" />
                                    </svg><span class=\"hidden\">.</span>
                                </span>
                                </a>
                            {% else %}
                                <a href=\"/user/login\" class=\"bg-gray-900 hover:bg-gray-800 px-3 py-2 mr-0.5 rounded-l-md text-sm font-medium\" aria-current=\"page\">
                                    <span class=\"text-white text-sm font-semibold\">
                                        <svg xmlns=\"http://www.w3.org/2000/svg\" class=\"h-5 w-5 mr-1 inline\" fill=\"none\" viewBox=\"0 0 24 24\" stroke=\"currentColor\">
                                            <path stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-width=\"2\" d=\"M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z\" />
                                        </svg>
                                        Se connecter
                                    </span>
                                </a>
                                <a href=\"/user/new\" class=\"bg-gray-900 hover:bg-green-600 pl-2 pr-1 py-2 rounded-r-md text-sm font-medium\" aria-current=\"page\">
                                <span class=\"text-white\">
                                    <svg xmlns=\"http://www.w3.org/2000/svg\" class=\"h-5 w-5 mr-1 inline\" fill=\"none\" viewBox=\"0 0 24 24\" stroke=\"currentColor\">
                                        <path stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-width=\"2\" d=\"M12 6v6m0 0v6m0-6h6m-6 0H6\" />
                                    </svg><span class=\"hidden\">.</span>
                                </span>
                                </a>
                            {% endif %}
                        </div>
                    </div>
                </div>
                <!-- Mobile menu, show/hide based on menu state. -->
                <div class=\"sm:hidden\" id=\"mobile-menu\">
                    <div class=\"px-2 pt-2 pb-3 space-y-1\">
                        <a href=\"/search\" class=\"text-gray-300 hover:bg-gray-900 hover:text-white block px-3 py-2 rounded-md text-base font-medium\">Recherche</a>
                        <a href=\"/travel\" class=\"text-gray-300 hover:bg-gray-900 hover:text-white block px-3 py-2 rounded-md text-base font-medium\">Trajets</a>
                    </div>
                </div>
            </nav>
        </header>
        {% block body %}{% endblock %}
    </body>
</html>
", "base.html.twig", "/var/www/symfony_travelexpress/templates/base.html.twig");
    }
}
