# TravelExpress

## Deploy with docker-compose 

To deploy this web application through Docker, please have as prerequisites installed `docker.io` and `docker-compose`

In project repository, run `docker-compose up --build -d`, through a terminal (PowerShell for Windows or any terminal for others distro), to build the images and deploy it.

After successful deployment, run `docker-compose exec php /bin/bash` to enter inside php container with a bash shell.

In the php container, run `php bin/console make:migration -q` and `php bin/console doctrine:migrations:migrate` to link the web application entities with the database container.

## Initialize fist data in database

At this point, you probably need to fill your database with at least cities.

When database is deployed, you need to fill it with city before make your test or push your web application in production.

Open your terminal in the project folder, depending on your OS, and run `docker-compose exec database /bin/bash` to enter insi database container with a bash shell.

In the database container, log into SQL database with `mysql -u root -p symfony_docker` with root password `secret` by default. Here the default table name is `symfony_docker`.

Once your working in the database table, insert datas with 

```sql
INSERT INTO `city` VALUES (1,'Montreal'),(2,'Quebec'),(3,'Laval'),(4,'Gatineau'),(5,'Longueuil'),(6,'Sherbrooke'),(7,'Levis'),(8,'Saguenay'),(9,'Trois-Rivieres'),(10,'Terrebonne'),(11,'Brossard'),(12,'Repentigny'),(13,'Saint-Jerome'),(14,'Drummondville'),(15,'Granby'),(16,'Blainville'),(17,'Saint-Hyacinthe'),(18,'Dollard-Des Ormeaux'),(19,'Mirabel');
```

or with another dataset values for your city.

## Start the server

For obscure reason, we need to give full permissions for `var/` directory. In the php container, run `chmod 777 -R var/` to give full permissions for everyone of this folder.

Finally, always in the php container, run `symfony server:start -d` to start the server.

Please notice to change `.env` file for specific purpose, like set environment to prod instead of dev.

